///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2022 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//  File:               RIOT_LGen_Definitions.ttcn
//  Description:
//  Rev:                R1B
//  Prodnr:             CNL 113 909
//  Updated:            2022-02-25
//  Contact:            http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: RIOT_LGen_Definitions
// 
//  Purpose:
//    This module contains definitions for RIoT's load generator component
// 
///////////////////////////////////////////////////////////////
module RIOT_LGen_Definitions
{
  import from IOT_LGen_Definitions all;
  
  import from RIOT_Config_Definitions all;
  import from RIOT_Database_Definitions all;
  import from RIOT_App_Database_Definitions all;
  import from RIOT_Northbound_Database_Definitions all;
  import from RIOT_App_LWM2M_FSM_Definitions all;
  import from RIOT_App_MQTT_FSM_Definitions all;
  import from RIOT_FSM_Definitions all;
  import from RIOT_Northbound_Leshan_FSM_Definitions all;
  import from RIOT_Variable_Definitions all;
  import from RIOT_Logger_Definitions all;
  
  import from EPTF_CLL_Base_Definitions all;
  import from EPTF_CLL_Common_Definitions all;
  import from EPTF_CLL_ExecCtrl_Definitions all;
  import from EPTF_CLL_FBQ_Definitions all;
  import from EPTF_CLL_LGenBase_Definitions all;
  
  import from EPTF_COAP_Transport_Definitions all;

  import from HTTPmsg_Types all;
  import from CoAP_Types all;

  modulepar boolean tsp_RIOT_InfluxDB_enabled := false;

  const charstring c_RIOT_Device_entityType := "RIOT_Device_Entity";
  const charstring c_RIOT_Northbound_entityType := "RIOT_Northbound_Entity";

  const charstring c_RIOT_Device_behaviorType := "RIOT_Device_Behavior";
  const charstring c_RIOT_Northbound_behaviorType := "RIOT_Northbound_Behavior";

  type component RIOT_LGen_CT extends IOT_LGen_CT
  {
    var RIOT_LGen_Config_List v_RIOT_config := tsp_RIOT_config;
    
    var charstring v_RIOT_LGen_name;
    var integer v_RIOT_LGen_idx;
    var integer v_RIOT_Device_bIdx;
    var integer v_RIOT_Northbound_bIdx;

    var RIOT_ApplicationData_DB           v_RIOT_ApplicationDatabase;
    var RIOT_ApplicationData              v_RIOT_ApplicationData;
    var integer                           v_RIOT_App_idx := -1;

    var RIOT_Northbound_DB                v_RIOT_NorthboundDatabase;
    var RIOT_Northbound_Data              v_RIOT_NorthboundData;
    var integer                           v_RIOT_Northbound_idx := -1;
    var HTTPMessage                       v_RIOT_Northbound_msgToSend;
    var integer                           v_RIOT_Northbound_appIdx := -1;
    
    var integer                           v_RIOT_HTTP_PortGroup2Entity_hashRef := -1;

    var RIOT_EGrpToConfig_DB              v_RIOT_EGrpToConfig_DB;
    var RIOT_EGrpToConfig                 v_RIOT_EGrpToConfig;

    var integer                           v_RIOT_EntityType := -1;

    var CoAP_Message                      v_RIOT_COAP_MSG;
    var EPTF_COAP_PDU                     v_RIOT_EPTF_COAP_PDU;

    // Local temp
    var EPTF_IntegerList                  v_RIOT_IntList := {};    
    var float                             v_RIOT_float;
    
    var RIOT_Variable_List                v_RIOT_varDB := {};
    var RIOT_FSM_Variables                v_RIOT_FSM_vars := c_RIOT_FSM_Variables_empty;
    
    var RIOT_Logger_CT		           v_RIOT_logger := null;
    var RIOT_Event                         v_RIOT_event := c_RIOT_Event_empty;
    var RIOT_EventVector                   v_RIOT_eventVector := {};
    var RIOT_Counters                      v_RIOT_counters := {};
    var RIOT_Counter                       v_RIOT_counter;
    port RIOT_Logging_PT                   LOGGER_PCO;
  }  

  type record RIOT_FSM_Variables
  {
    RIOT_Common_FSM_Smartmetering_Variables commonSmartmetering,
    RIOT_Common_FSM_Mqtt_Variables          commonMqtt,
    RIOT_Leshan_FSM_Variables               leshanNorthbound
  }
  
  const RIOT_FSM_Variables c_RIOT_FSM_Variables_empty :=
  {
    commonSmartmetering := c_RIOT_Common_FSM_Smartmetering_Variables_empty,
    commonMqtt := c_RIOT_Common_FSM_Mqtt_Variables_empty,
    leshanNorthbound := c_RIOT_Leshan_FSM_Variables_empty
  }
  
  // ENB event
  const integer    c_RIOT_eventIdx_COAP_transportEstablished := 0;
  const charstring c_RIOT_eventName_COAP_transportEstablished := "RIOT ENB COAP: transportEstablished"; // entity level

  const integer    c_RIOT_eventIdx_stopEntity := 1;
  const charstring c_RIOT_eventName_stopEntity := "RIOT ENB: stop entity signal"; // entity level

  const integer    c_RIOT_eventIdx_lwm2mReregister := 2;
  const charstring c_RIOT_eventName_lwm2mReregister := "RIOT ENB: LwM2M reregister signal"; // entity level

  const integer    c_RIOT_eventIdx_lwm2mNotification := 3;
  const charstring c_RIOT_eventName_lwm2mNotification := "RIOT ENB: LwM2M notification signal"; // entity level

  // Northbound events
  const integer    c_RIOT_eventIdx_Pool_Reserve_Succ := 0;
  const charstring c_RIOT_eventName_Pool_Reserve_Succ := "RIOT ENB POOL: Reserve Success"; // fsm level

  const integer    c_RIOT_eventIdx_Pool_Reserve_Fail := 1;
  const charstring c_RIOT_eventName_Pool_Reserve_Fail := "RIOT ENB POOL: Reserve Failure"; // fsm level
  
  const integer    c_RIOT_eventIdx_Northbound_retryAttemptsExceeded := 2;
  const charstring c_RIOT_eventName_Northbound_retryAttemptsExceeded := "RIOT ENB Northbound: retryAttemptsExceeded"; // fsm level
  
  const integer    c_RIOT_eventIdx_Northbound_jobStatusMatch := 3;
  const charstring c_RIOT_eventName_Northbound_jobStatusMatch := "RIOT ENB Northbound: jobStatusMatch"; // fsm level
  
  const integer    c_RIOT_eventIdx_Northbound_jobStatusMismatch := 4;
  const charstring c_RIOT_eventName_Northbound_jobStatusMismatch := "RIOT ENB Northbound: jobStatusMismatch"; // fsm level
  
  const integer    c_RIOT_eventIdx_Northbound_firmwareDownloaded := 5;
  const charstring c_RIOT_eventName_Northbound_firmwareDownloaded := "RIOT ENB Northbound: firmwareDownloaded"; // entity level
  
  const integer    c_RIOT_eventIdx_Northbound_firmwareUpdated := 6;
  const charstring c_RIOT_eventName_Northbound_firmwareUpdated := "RIOT ENB Northbound: firmwareUpdated"; // entity level  
}
