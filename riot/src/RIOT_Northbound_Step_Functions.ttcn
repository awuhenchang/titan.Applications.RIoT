///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2022 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//  File:               RIOT_Northbound_Step_Functions.ttcn
//  Description:
//  Rev:                R1B
//  Prodnr:             CNL 113 909
//  Updated:            2022-02-25
//  Contact:            http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////
module RIOT_Northbound_Step_Functions
{
  import from RIOT_Northbound_Step_Definitions all;
  import from RIOT_Northbound_Leshan_Step_Functions all;
  import from RIOT_Northbound_Database_Functions all;
  import from IOT_LGen_Functions all;
  import from IOT_LGen_Steps all;
  import from RIOT_Config_Definitions all;
  import from RIOT_LGen_Definitions all;
  import from RIOT_Database_Functions all;
  import from RIOT_Logger_Definitions all;
  import from RIOT_Step_Functions all;
  import from RIOT_App_Step_Functions all;
  import from EPTF_CLL_Common_Definitions all;
  import from EPTF_CLL_LGenBase_Definitions all;
  import from EPTF_CLL_LGenBase_ConfigFunctions all;
  import from EPTF_CLL_LGenBase_EventHandlingFunctions all;
  import from EPTF_CLL_Variable_Definitions all;
  import from EPTF_CLL_Variable_Functions all;
  import from EPTF_CLL_FBQ_Functions all;
  import from EPTF_HTTP_Definitions all;
  import from EPTF_HTTP_Functions all;
  import from EPTF_HTTP_Transport_Definitions all;
  import from EPTF_HTTP_Transport_Functions all;
  import from HTTPmsg_Types all;
  import from JSON_Types all;

  function f_RIOT_Northbound_declareSteps()
  runs on RIOT_LGen_CT
  {
    f_EPTF_LGenBase_declareStep(c_RIOT_Northbound_behaviorType, {c_RIOT_Northbound_stepName_init, 
      refers(f_RIOT_Northbound_step_init)}
    );
    f_EPTF_LGenBase_declareStep(c_RIOT_Northbound_behaviorType, {c_RIOT_Northbound_stepName_cleanUp, 
      refers(f_RIOT_Northbound_step_cleanUp)}
    );
    f_EPTF_LGenBase_declareStep(c_RIOT_Northbound_behaviorType, {c_RIOT_Northbound_stepName_HTTP_setActiveFsmForEntity, 
      refers(f_RIOT_Northbound_step_HTTP_setActiveFsmForEntity)}
    );
    f_EPTF_LGenBase_declareStep(c_RIOT_Northbound_behaviorType, {c_RIOT_Northbound_stepName_HTTP_newPortGroup, 
      refers(f_RIOT_Northbound_step_HTTP_newPortGroup)}
    );
    f_EPTF_LGenBase_declareStep(c_RIOT_Northbound_behaviorType, {c_RIOT_Northbound_stepName_HTTP_deletePortGroup, 
      refers(f_RIOT_Northbound_step_HTTP_deletePortGroup)}
    );
    f_EPTF_LGenBase_declareStep(c_RIOT_Northbound_behaviorType, {c_RIOT_Northbound_stepName_HTTP_setConnectionKeepAlive, 
      refers(f_RIOT_Northbound_step_HTTP_setConnectionKeepAlive)}
    );
    f_EPTF_LGenBase_declareStep(c_RIOT_Northbound_behaviorType, {c_RIOT_Northbound_stepName_HTTP_setConnectionClose, 
      refers(f_RIOT_Northbound_step_HTTP_setConnectionClose)}
    );
    f_EPTF_LGenBase_declareStep(c_RIOT_Northbound_behaviorType, {c_RIOT_Northbound_stepName_POOL_reserveFromNorthboundAvailablePool, 
      refers(f_RIOT_App_step_reserveFromNorthboundAvailablePool)}
    );
    f_EPTF_LGenBase_declareStep(c_RIOT_Northbound_behaviorType, {c_RIOT_Northbound_stepName_POOL_releaseFromReservedPool, 
      refers(f_RIOT_App_step_releaseFromReservedPool)}
    );
    f_EPTF_LGenBase_declareStep(c_RIOT_Northbound_behaviorType, {c_RIOT_Northbound_stepName_log_event, 
      refers(f_RIOT_Northbound_step_log_event)}
    );
    f_EPTF_LGenBase_declareStep(c_RIOT_Northbound_behaviorType, {c_RIOT_Northbound_stepName_log_timer, 
      refers(f_RIOT_Northbound_step_log_timer)}
    );
    f_EPTF_LGenBase_declareStep(c_RIOT_Northbound_behaviorType, {c_RIOT_Northbound_stepName_log_verdict, 
      refers(f_RIOT_Northbound_step_log_verdict)}
    );
    f_EPTF_LGenBase_declareStep(c_RIOT_Northbound_behaviorType, {c_RIOT_Northbound_stepName_log_finished, 
      refers(f_RIOT_Northbound_step_log_finished)}
    );

    f_RIOT_Northbound_Leshan_declareSteps();
    
    f_EPTF_HTTP_setSearchContextFunction(refers(f_RIOT_Northbound_searchServerContext));
  }

  function f_RIOT_Northbound_isEntityInitialized(in integer p_eIdx)
  runs on RIOT_LGen_CT
  return boolean
  {
    v_RIOT_IntList := f_EPTF_LGenBase_getBehaviorCtx(p_eIdx, v_RIOT_Northbound_bIdx);

    if (sizeof(v_RIOT_IntList) >= 1 and v_RIOT_IntList[0] >= 0) { return true; }    

    return false;
  }
  
  function f_RIOT_Northbound_setStepContext(in EPTF_LGenBase_TestStepArgs pl_ptr)
  runs on RIOT_LGen_CT
  return boolean
  {
    return f_RIOT_Northbound_setContext(pl_ptr.eIdx);
  }
  
  function f_RIOT_Northbound_setContext(in integer p_eIdx)
  runs on RIOT_LGen_CT
  return boolean
  {    
    v_RIOT_IntList := f_EPTF_LGenBase_getBehaviorCtx(p_eIdx, v_RIOT_Northbound_bIdx);

    if (sizeof(v_RIOT_IntList) < 1)
    { 
      f_IOT_LGen_Logging_WARNING(log2str(%definitionId, " entity was not initialized! ",p_eIdx));
      return false; 
    }

    v_RIOT_Northbound_idx := v_RIOT_IntList[0];
    
    if (v_RIOT_Northbound_idx >= 0 and v_RIOT_Northbound_idx < sizeof(v_RIOT_NorthboundDatabase.data))
    {
      v_RIOT_Northbound_appIdx := v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].appIdx;
    }
    
    return true;
  }
  
  function f_RIOT_Northbound_step_init(
      in EPTF_LGenBase_TestStepArgs pl_ptr)
    runs on RIOT_LGen_CT
  {
    f_IOT_LGen_Logging_DEBUG(log2str(%definitionId, pl_ptr));

    if (f_RIOT_Northbound_isEntityInitialized(pl_ptr.eIdx))
    {
      f_RIOT_Northbound_setStepContext(pl_ptr);
      if (v_RIOT_Northbound_idx >= 0) 
      {
        f_RIOT_Northbound_Database_remove(v_RIOT_NorthboundDatabase, v_RIOT_Northbound_idx);
      }
    }

    f_RIOT_Northbound_Data_generate(v_RIOT_NorthboundDatabase, pl_ptr.eIdx, v_RIOT_NorthboundData, v_RIOT_config);

    f_IOT_LGen_Logging_DEBUG(log2str(" generated northbound data: ", v_RIOT_NorthboundData));

    v_RIOT_Northbound_idx := f_RIOT_Northbound_Database_add(v_RIOT_NorthboundDatabase, v_RIOT_NorthboundData);

    v_RIOT_IntList := {v_RIOT_Northbound_idx};
    
    f_EPTF_LGenBase_setBehaviorCtx(pl_ptr.eIdx, v_RIOT_Northbound_bIdx, v_RIOT_IntList);
    
    f_EPTF_HTTP_setEntityContext(
      pl_eIdx := pl_ptr.eIdx,
      pl_method := "GET", pl_uri := "",
      pl_version_major := 1, pl_version_minor := 1,
      pl_headerlines := {{"Connection","Keep-Alive"}},
      pl_connId := -1, //v_HTTP_portGroupConnectionHandle,
      pl_authDetails := -,    
      pl_body := { charVal := "" });
    
    f_IOT_LGen_Logging_DEBUG(log2str(" entity ctx: ", v_RIOT_IntList));
  }  
  
  function f_RIOT_Northbound_step_cleanUp(
      in EPTF_LGenBase_TestStepArgs pl_ptr)
    runs on RIOT_LGen_CT
  {
    f_IOT_LGen_Logging_DEBUG(log2str(%definitionId, pl_ptr));
     
    if (f_RIOT_Northbound_isEntityInitialized(pl_ptr.eIdx))
    {
      f_RIOT_Northbound_setStepContext(pl_ptr);
      if (v_RIOT_Northbound_idx > 0) 
      {
        f_RIOT_Northbound_Database_remove(v_RIOT_NorthboundDatabase, v_RIOT_Northbound_idx);
        f_EPTF_LGenBase_setBehaviorCtx(pl_ptr.eIdx, v_RIOT_Northbound_bIdx, {});
      }
    }
  }
  
  function f_RIOT_Northbound_step_HTTP_newPortGroup(in EPTF_LGenBase_TestStepArgs pl_ptr)
  runs on RIOT_LGen_CT
  {
    f_IOT_LGen_Logging_DEBUG(log2str(%definitionId, pl_ptr));

    if (not f_RIOT_Northbound_setStepContext(pl_ptr)) { return }

    if (v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].portGroupId == -1)
    {
      v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].portGroupId := f_EPTF_HTTP_LocalTransport_newPort({
        name := "Northbound_httpPort_"&int2str(pl_ptr.eIdx),
        localHostInformation := { v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].localHttp.hostName, v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].localHttp.portNumber },
        remoteHostInformation := { v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].remoteHttp.hostName, v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].remoteHttp.portNumber },
        instantConnOpen := true,
        instantConnClose := false, 
        useSSL := v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].useSSL, //true 
        userFunctions := omit
      });
      
      f_EPTF_HTTP_setEntityContextConnectionId(pl_ptr.eIdx, v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].portGroupId);
      
      f_RIOT_HTTP_PortGroup2Entity_add(
	v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].portGroupId,
	pl_ptr.eIdx
      );
      
      v_RIOT_NorthboundDatabase.connections := v_RIOT_NorthboundDatabase.connections + 1;
    }
  }
  
  function f_RIOT_Northbound_step_HTTP_deletePortGroup(in EPTF_LGenBase_TestStepArgs pl_ptr)
  runs on RIOT_LGen_CT
  {
    f_IOT_LGen_Logging_DEBUG(log2str(%definitionId, pl_ptr));

    if (not f_RIOT_Northbound_setStepContext(pl_ptr)) { return }

    if (v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].portGroupId != -1)
    {
      f_EPTF_HTTP_LocalTransport_closeConnection_defaultGroup(v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].portGroupId);
      
      f_EPTF_HTTP_LocalTransport_deletePortGroup(v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].portGroupId);
      
      f_RIOT_HTTP_PortGroup2Entity_add(
	v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].portGroupId,
	-1
      );
      
      v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].portGroupId := -1;
      
      v_RIOT_NorthboundDatabase.connections := v_RIOT_NorthboundDatabase.connections - 1;
    }
  }
  
  function f_RIOT_Northbound_step_HTTP_setActiveFsmForEntity(in EPTF_LGenBase_TestStepArgs pl_ptr)
  runs on RIOT_LGen_CT  
  {
    v_EPTF_HTTP_ConnId2FIdx[pl_ptr.eIdx] := pl_ptr.refContext.fCtxIdx;
  }
  
  function f_RIOT_Northbound_step_HTTP_setConnectionKeepAlive(in EPTF_LGenBase_TestStepArgs pl_ptr)
  runs on RIOT_LGen_CT
  {
    f_IOT_LGen_Logging_DEBUG(log2str(%definitionId));

    if (not f_RIOT_Northbound_setStepContext(pl_ptr)) { return }

    v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].connection := "keep_alive";
  }

  function f_RIOT_Northbound_step_HTTP_setConnectionClose(in EPTF_LGenBase_TestStepArgs pl_ptr)
  runs on RIOT_LGen_CT
  {
    f_IOT_LGen_Logging_DEBUG(log2str(%definitionId));

    if (not f_RIOT_Northbound_setStepContext(pl_ptr)) { return }

    v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].connection := "close";
  }

  function f_RIOT_Northbound_step_HTTP_loadRequestIntoEntityContext(in EPTF_LGenBase_TestStepArgs pl_ptr)
  runs on RIOT_LGen_CT
  {
    f_IOT_LGen_Logging_DEBUG(log2str(%definitionId));

    var integer vl_EPTF_HTTP_myCtx := f_EPTF_HTTP_getContextIndex(pl_ptr.eIdx);
    v_EPTF_HTTP_contexts[vl_EPTF_HTTP_myCtx].method := v_RIOT_Northbound_msgToSend.request.method;
    v_EPTF_HTTP_contexts[vl_EPTF_HTTP_myCtx].uri := v_RIOT_Northbound_msgToSend.request.uri;
    v_EPTF_HTTP_contexts[vl_EPTF_HTTP_myCtx].version_major := v_RIOT_Northbound_msgToSend.request.version_major;
    v_EPTF_HTTP_contexts[vl_EPTF_HTTP_myCtx].version_minor := v_RIOT_Northbound_msgToSend.request.version_minor;
    v_EPTF_HTTP_contexts[vl_EPTF_HTTP_myCtx].headerGetterFn := refers(f_RIOT_Northbound_getHeaderContent);
    v_EPTF_HTTP_contexts[vl_EPTF_HTTP_myCtx].contentGetterFn := refers(f_RIOT_Northbound_getBodyContent);
  }

  function f_RIOT_App_step_reserveFromNorthboundAvailablePool(in EPTF_LGenBase_TestStepArgs pl_ptr)
  runs on RIOT_LGen_CT
  {
    f_IOT_LGen_Logging_DEBUG(log2str(%definitionId));

    if (not f_RIOT_Northbound_setStepContext(pl_ptr)) { return }

    var integer v_availableIdx := -1;
    var integer v_appIdx := -1;

    // If we have elements already in northboundAvailableQueue
    if (f_EPTF_FBQ_getLengthOfBusyChain(v_RIOT_ApplicationDatabase.northboundAvailableQueue) > 0)
    {
      // Get the 1st element from the northboundAvailableQueue      
      if(f_EPTF_FBQ_getBusyHeadIdx(v_availableIdx, v_RIOT_ApplicationDatabase.northboundAvailableQueue))
      {
        // Create the association from northbound instance -> application instance
	v_appIdx := v_RIOT_ApplicationDatabase.northboundAvailableList[v_availableIdx];
        v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].appIdx := v_appIdx;
        
        // Remove the entry from the northboundAvailableQueue
	v_RIOT_ApplicationDatabase.northboundAvailableList[v_availableIdx] := -1;
        f_EPTF_FBQ_moveFromBusyToFreeTail(v_availableIdx, v_RIOT_ApplicationDatabase.northboundAvailableQueue);

        action(v_appIdx, " removed from AVAILABLE POOL ", f_EPTF_FBQ_getLengthOfBusyChain(v_RIOT_ApplicationDatabase.northboundAvailableQueue));

        // Create entry in the reserved pool
        var integer v_reservedIdx := f_EPTF_FBQ_getOrCreateFreeSlot(v_RIOT_NorthboundDatabase.northboundReservedQueue);
        f_EPTF_FBQ_moveFromFreeHeadToBusyTail(v_RIOT_NorthboundDatabase.northboundReservedQueue);
        f_IOT_LGen_Logging_DEBUG(log2str(": "," adding northbound instance idx at ", v_reservedIdx, " nb reference idx: ", v_RIOT_Northbound_idx));

        v_RIOT_NorthboundDatabase.northboundReservedList[v_reservedIdx] := v_RIOT_Northbound_idx;
        v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].reservedPoolIdx := v_reservedIdx;
        
        // Create the association from application instance -> northbound instance
        v_RIOT_ApplicationDatabase.data[v_appIdx].availablePoolIdx := -1;
        v_RIOT_ApplicationDatabase.data[v_appIdx].northboundIdx := v_RIOT_Northbound_idx;
        v_RIOT_ApplicationDatabase.data[v_appIdx].northboundState := RESERVED;
        
        action(v_RIOT_Northbound_idx, " added to RESERVED POOL ", f_EPTF_FBQ_getLengthOfBusyChain(v_RIOT_NorthboundDatabase.northboundReservedQueue));
        
        // Dispatching SUCC event for caller FSM
        f_EPTF_LGenBase_postEvent(
          {
            {
              v_RIOT_Northbound_bIdx,
              c_RIOT_eventIdx_Pool_Reserve_Succ,
              {
                pl_ptr.eIdx,
                pl_ptr.refContext.fCtxIdx
              }, omit
            },
            {}
        });
        return;
      }
    }
    
    // Not successful reservation: Dispatching FAIL event for caller FSM
    action(" AVAILABLE POOL is empty ", f_EPTF_FBQ_getLengthOfBusyChain(v_RIOT_ApplicationDatabase.northboundAvailableQueue));
    f_EPTF_LGenBase_postEvent(
      {
        {
          v_RIOT_Northbound_bIdx,
          c_RIOT_eventIdx_Pool_Reserve_Fail,
          {
            pl_ptr.eIdx,
            pl_ptr.refContext.fCtxIdx
          }, omit
        },
        {}
    });
  }
  
  function f_RIOT_App_step_releaseFromReservedPool(in EPTF_LGenBase_TestStepArgs pl_ptr)
  runs on RIOT_LGen_CT
  {
    f_IOT_LGen_Logging_DEBUG(log2str(%definitionId));

    if (not f_RIOT_Northbound_setStepContext(pl_ptr)) { return }

    var integer v_app_eIdx := v_RIOT_ApplicationDatabase.data[v_RIOT_Northbound_appIdx].eIdx;

    // Remove from reserved pool    
    if (f_RIOT_Pool_removeFromNorthboundReserved(pl_ptr.eIdx))
    {      
      // Check availability conditions
      if (f_RIOT_checkNorthboundAvailableConditions(v_app_eIdx))
      {
        // Add to available pool
	f_RIOT_Pool_addToNorthboundAvailable(v_app_eIdx);   
      }
      //else
      //{
      //  f_RIOT_Pool_removeFromNorthboundAvailable(v_app_eIdx);
      //}  
    }    
  }
  
  function f_RIOT_Pool_removeFromNorthboundReserved(in integer pl_eIdx)
  runs on RIOT_LGen_CT
  return boolean
  {
    f_IOT_LGen_Logging_DEBUG(log2str(%definitionId));

    if (not f_RIOT_Northbound_setContext(pl_eIdx)) { return false; }

    var integer v_reservedIdx := v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].reservedPoolIdx;
    var integer v_appIdx := v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].appIdx;
    var integer v_app_eIdx := v_RIOT_ApplicationDatabase.data[v_RIOT_Northbound_appIdx].eIdx;
    
    // Remove from reserved pool    
    if (v_reservedIdx >= 0 and v_appIdx >= 0)
    {
      v_RIOT_NorthboundDatabase.northboundReservedList[v_reservedIdx] := -1;
      f_EPTF_FBQ_moveFromBusyToFreeHead(v_reservedIdx, v_RIOT_NorthboundDatabase.northboundReservedQueue);
      
      action(v_RIOT_Northbound_idx, " removed from RESERVED POOL ", f_EPTF_FBQ_getLengthOfBusyChain(v_RIOT_NorthboundDatabase.northboundReservedQueue));

      // Remove associations
      v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].reservedPoolIdx := -1;
      v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].appIdx := -1;
      v_RIOT_ApplicationDatabase.data[v_RIOT_Northbound_appIdx].northboundState := UNAVAILABLE;
      
      v_RIOT_ApplicationDatabase.data[v_appIdx].northboundIdx := -1;
      
      return true;
    }
    
    return false;
  }
  
  function f_RIOT_Northbound_step_log_event(in EPTF_LGenBase_TestStepArgs pl_ptr)
  runs on RIOT_LGen_CT
  {

    f_IOT_LGen_Logging_DEBUG(log2str(%definitionId));

    if (not f_RIOT_Northbound_setStepContext(pl_ptr)) { return }

    v_RIOT_event := c_RIOT_Event_empty;
    v_RIOT_event.timestamp := T_EPTF_componentClock.read;
    v_RIOT_event.direction := I;
    v_RIOT_event.protocol := EVENT;
    v_RIOT_event.msg := 
      f_EPTF_LGenBase_getFsmNameByStepArgs(pl_ptr) & ": " &
      f_EPTF_LGenBase_getFsmStateNameByStepArgs(pl_ptr) & ": " &
      f_EPTF_LGenBase_iIdx2Str(
        pl_ptr.reportedEvent.event.bIdx, 
        pl_ptr.reportedEvent.event.iIdx,
        f_EPTF_LGenBase_getFsmIndexByCtxIdx(pl_ptr.eIdx, pl_ptr.refContext.fCtxIdx));

    v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].eventVector.events[sizeof(v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].eventVector.events)] :=
      v_RIOT_event;
  }
  
  function f_RIOT_Northbound_step_log_timer(in EPTF_LGenBase_TestStepArgs pl_ptr)
  runs on RIOT_LGen_CT
  {
    f_IOT_LGen_Logging_DEBUG(log2str(%definitionId));

    if (not f_RIOT_Northbound_setStepContext(pl_ptr)) { return }
   
    v_RIOT_event := c_RIOT_Event_empty;
    v_RIOT_event.timestamp := T_EPTF_componentClock.read;
    v_RIOT_event.direction := I;
    v_RIOT_event.protocol := EVENT;
    v_RIOT_event.msg := 
      f_EPTF_LGenBase_getFsmNameByStepArgs(pl_ptr) & ": " &
      f_EPTF_LGenBase_getFsmStateNameByStepArgs(pl_ptr) & ": " &
      f_EPTF_LGenBase_getNameOfExpiredTimer(pl_ptr);
      
    v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].eventVector.events[sizeof(v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].eventVector.events)] :=
      v_RIOT_event;
  }
  
  function f_RIOT_Northbound_step_log_verdict(in EPTF_LGenBase_TestStepArgs pl_ptr)
  runs on RIOT_LGen_CT
  {
    f_IOT_LGen_Logging_DEBUG(log2str(%definitionId));

    if (not f_RIOT_Northbound_setStepContext(pl_ptr)) { return }
    
    v_RIOT_event := c_RIOT_Event_empty;
    v_RIOT_event.timestamp := T_EPTF_componentClock.read;
    v_RIOT_event.direction := I;
    v_RIOT_event.protocol := VERDICT;
    v_RIOT_event.msg := 
      f_EPTF_LGenBase_getFsmNameByStepArgs(pl_ptr) & ": " &
      f_EPTF_LGenBase_charstringValOfStep(pl_ptr);
      
    v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].eventVector.events[sizeof(v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].eventVector.events)] :=
      v_RIOT_event;
  }
  
  function f_RIOT_Northbound_step_log_finished(in EPTF_LGenBase_TestStepArgs pl_ptr)
  runs on RIOT_LGen_CT
  {
    f_IOT_LGen_Logging_DEBUG(log2str(%definitionId));

    if (not f_RIOT_Northbound_setStepContext(pl_ptr)) { return }

    LOGGER_PCO.send(v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].eventVector);

    v_RIOT_NorthboundDatabase.data[v_RIOT_Northbound_idx].eventVector.events := {};
  }

  function f_RIOT_Northbound_getHeaderContent(
    in EPTF_IntegerList pl_pars,
    inout HeaderLines pl_headerLines)
  runs on RIOT_LGen_CT
  {
    if (ischosen(v_RIOT_Northbound_msgToSend.request))
    {
      pl_headerLines := v_RIOT_Northbound_msgToSend.request.header;
    }
    else { f_IOT_LGen_Logging_WARNING(log2str(%definitionId, " message should be request!")); }
  }

  function f_RIOT_Northbound_getBodyContent(
    in EPTF_IntegerList pl_pars,
    inout EPTF_HTTP_CharOrOct pl_body)
  runs on RIOT_LGen_CT
  {
    if (ischosen(v_RIOT_Northbound_msgToSend.request))
    {
      pl_body := { charVal := v_RIOT_Northbound_msgToSend.request.body };
    }
    else { f_IOT_LGen_Logging_WARNING(log2str(%definitionId, " message should be request!")); }
  }
  
  function f_RIOT_Northbound_searchServerContext(inout integer pl_eIdx, inout integer pl_fsmCtx)
  runs on RIOT_LGen_CT
  {
    f_IOT_LGen_Logging_DEBUG(log2str(%definitionId));
    
    f_IOT_LGen_Logging_DEBUG(log2str("v_EPTF_HTTP_incomingMessage: ", v_EPTF_HTTP_incomingMessage));
    f_IOT_LGen_Logging_DEBUG(log2str("v_EPTF_HTTP_lastReceivedGroupId: ", v_EPTF_HTTP_lastReceivedGroupId));
    f_IOT_LGen_Logging_DEBUG(log2str("v_EPTF_HTTP_lastReceivedPortId: ", v_EPTF_HTTP_lastReceivedPortId));
    f_IOT_LGen_Logging_DEBUG(log2str("v_EPTF_HTTP_lastReceivedSeqNum: ", v_EPTF_HTTP_lastReceivedSeqNum));

  }
}
